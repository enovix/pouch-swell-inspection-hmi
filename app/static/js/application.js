$(document).ready(function(){
    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port);

    socket.on('connect', function() {
		console.log('User has connected!');
	});

    socket.on('message', function(msg) {
		console.log('Received message: ' + msg);
	});

    //Socket receive emit update events
    socket.on('update', function(res) {
        res.forEach(function(element) {
            element = JSON.parse(element);
            $(element.id).replaceWith(element.html);
//            console.log(element)
        })
    });



    socket.on('updateAttr', function(res) {
        res.forEach(function(element) {
            element = JSON.parse(element);
            if (element.attribute == 'text') {
			    $(element.selector).text(element.value);
			} if (element.attribute == 'css') {
			    $(element.selector).css(element.value[0], element.value[1])
			} else {
			    $(element.selector).attr(element.attribute, element.value);
			}
//			console.log(element)
        })
    });

    //Socket receive emit modal event
    socket.on('modal', function(res) {
//        console.log(res)
        data = JSON.parse(res)

        if (data.command == 'static') {
            $(data.id).data('bs.modal', null)
            $(data.id).modal({backdrop: 'static', keyboard: false})
        } else {
            $(data.id).modal(data.command)
        }
    });

    socket.on('disable', function(res) {
        console.log(res)
        data = JSON.parse(res)

        if (!!data.id){
            if (data.state == "false") {
                $(data.id).prop('disabled', false);
                console.log('enabling button')
            } else {
                $(data.id).prop('disabled', true);
            }
        }
    });

    $(document).on('click', '.modal [type="modal-submit"]', function() {
        p = $(this).parents('.modal-content');
        var res = {};
        $(p).find('*').each(function (indx, element) {
            if (!!element.id){
                res[element.id] = $(element).val();
            }
        })
        socket.emit('modal_submit_'+this.id, JSON.stringify(res))
    })

    $(document).on('click', '.TSM button', function() {
        socket.emit('TSM_btnClick', $(this).parents('.input-group').attr('id'), $(this).attr('btn-type'))
	});

    //Socket emit click event
	$(document).on('click', "*", function() {
	    if (!!this.id){
	        if (Object.keys($(this).data()).length != 0 ) {
	            socket.emit('trigger_'+this.id, $(this).data());
//	            console.log('clicked with data: ' + $(this).data())
	        } else {
	            socket.emit('trigger_'+this.id);
//	            console.log('clicked ' + this.id)
	        }
	    }
	});

    //Socket emit change event
    $(document).on('change', '*', function() {
        if (!!this.id) {
            socket.emit('change_'+ this.id, $(this).find("option:selected").text())
//            console.log('change event call for: ' + this.id)
//            console.log('changed value: ' + $(this).find("option:selected").text())
        }
    });

	$(document).on('change', 'input', function() {
        if (!!this.id) {
//			console.log(this.value)
            socket.emit('inputchange_'+ this.id, this.value)
        }
    });

    $("form").on('submit', function() {
        var selected = []
        var formID = $(this).attr("id")
//        console.log($(this))
        $(this).children(".form-control").each(function(){
            selected.push($(this).val())
//            console.log(selected)
        })
        socket.emit('submit_'+formID, selected)
    });
});