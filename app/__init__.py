from flask import Flask
from flask_socketio import SocketIO
import config
import logging



# initialize flask app
app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'

if config.ENVIRONMENT in ['dev2']:
	app.config['ENV'] = 'development'
	app.debug = True

if config.ENVIRONMENT in ['dev1']:
	app.debug = False

# Turn off logging of every API call
# Only displays Errors
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

socketio = SocketIO(app)

# load views
from app import views

# load config
app.config.from_object('config')