import config, actions, json
from flask import render_template
from flask_socketio import send
from app import app, socketio
from opc_ua.objects import opc_ua_server
from actions import tsm, uaw


# ==== Pages ==== #
@app.route('/')
@app.route('/main')
def index():
	return render_template("main.html")

@app.route('/chart')
def motion():
	chart_url = f'{config.DASH_HOST}:{config.DASH_PORT}'
	test_query = ''
	channel_query = ''

	for i, test in enumerate(uaw.open_test_names):
		if test:
			if not channel_query:
				channel_query = f'&channel=channel{i+1}'
				test_query = '?test_ids=' + uaw.open_test_names[i]
			else:
				test_query = test_query + '%' + uaw.open_test_names[i]

	return render_template("chart.html", chart_url='http://'+chart_url+'/dash/PSI'+test_query+channel_query)


@app.route('/vision')
def vision():
	return render_template("vision.html")

@app.route('/recipe')
def recipe():
	return render_template("recipe.html", recipes='<tr><td>poop</td></tr>')

@app.route('/close')
def close_opc_ua_connection():
	actions.CURRENT_PAGE = 'main'
	opc_ua_server.disconnect()

	return render_template("main.html")

# ==== Dev Shtaff ==== #
@socketio.on('test')
def debug_click_action(*args, **kwargs):
	print(args)
	a = args
	b = kwargs
	test = 1

@socketio.on('btnClick_inputGroupChannel1')
def test(*args):
	for arg in args:
		print(arg)


# ==== Socket Commands ==== #
@socketio.on('message')
def handleMessage(msg):
	print('socketio: Message: ' + msg)
	send(msg, broadcast=True)

@socketio.on('connect')
def socketio_connect():
	if opc_ua_server.is_connected():
		actions.opc_ua_refresh()
		print('socketio: Client connected')
	else:
		print('OPC UA Server disconnected')

@socketio.on('disconnect')
def socketio_disconnect():
	print('socketio: Client disconnected')

## TSM commands ##
@socketio.on('TSM_btnClick')
def tsm_click_action(group_id, btn_type):
	groups = ['inputGroupChannel1',
			  'inputGroupChannel2',
			  'inputGroupChannel3',
			  'inputGroupChannel4',
			  'inputGroupChannel5',
			  'inputGroupChannel6',
			  'inputGroupChannel7',
			  'inputGroupChannel8',
			  'inputGroupChannel9',
			  'inputGroupChannel10',
			  'inputGroupChannel11',
			  'inputGroupChannel12',
			  'inputGroupChannel13',
			  'inputGroupChannel14',
			  'inputGroupChannel15',
			  'inputGroupChannel16']

	try:
		sensor_id = groups.index(group_id)
	except NameError:
		print(f'group_id: "{group_id}" does not exist.')
		return

	if 'initiate' == btn_type:
		print(f'sending home trigger to sensor [{sensor_id}]')
		opc_ua_server.set_value(f'ns=4;s=MAIN.sensors[{sensor_id}].trig_home', True)
	elif 'start' == btn_type:
		print(f'sending run trigger to sensor [{sensor_id}]')
		opc_ua_server.set_value(f'ns=4;s=MAIN.sensors[{sensor_id}].trig_run', True)
	elif 'pause' == btn_type:
		print(f'sending pause trigger to sensor [{sensor_id}]')
		opc_ua_server.set_value(f'ns=4;s=MAIN.sensors[{sensor_id}].trig_pause', True)
	elif 'abort' == btn_type:
		print(f'sending stop trigger to sensor [{sensor_id}]')
		opc_ua_server.set_value(f'ns=4;s=MAIN.sensors[{sensor_id}].trig_stop', True)
	elif 'lockTSM' == btn_type:
		if tsm[sensor_id].lock_tsm_actions:
			opc_ua_server.set_value(f'ns=4;s=MAIN.sensors[{sensor_id}].inp_lock_state', False)
		else:
			opc_ua_server.set_value(f'ns=4;s=MAIN.sensors[{sensor_id}].inp_lock_state', True)
	else:
		print(f'failed to find tsm button type {btn_type}')


#===Recipe Manager Commands===#
#===UAW commands===#
@socketio.on('modal_submit_uawSetTestID')
def submit_test_id(json_str):
	data = json.loads(json_str)
	print(data)

	channel_number = data['uawChannelSelect']
	test_id = data['uawTestID']

	if len(test_id) > 0:
		opc_ua_server.set_value(f'ns=4;s=MAIN.sensors[{channel_number}].hmi.inp_test_name', test_id)
		opc_ua_server.set_value(f'ns=4;s=MAIN.sensors[{channel_number}].hmi.inp_submit_test_name', True)
	else:
		print(f'Test Name {test_id} is invalid')

#===motion manager===#
#===TVS Commands===#
@socketio.on('trigger_btnConnectionToggle')
def btnConnectionToggle():
	if opc_ua_server.is_connected():
		opc_ua_server.disconnect()
	else:
		opc_ua_server.connect()
		if opc_ua_server.is_connected():
			actions.resubscribe_opc_tags()
			actions.opc_ua_refresh()
