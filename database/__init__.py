import mysql.connector, json
# from pypika import Query, Table, Field
import config
import os


class DataBase:
	# def get_inspection_summary_complete(self, roll_id, sample_size):
	def __init__(self):
		# read creds.txt
		os.chdir(r'C:\Users\Public\Documents\caoffset-hmi')
		with open('..\creds.txt') as f:
			creds = json.load(f)
		self.db_host = config.db_host
		self.db_default_db = config.db_default_db
		self.db_user = creds['db_user']
		self.db_password = creds['db_password']
		self.conn = None
		self.curr = None
		self.frame_data = {}

	def connect(self):
		self.conn = mysql.connector.connect(
			host=self.db_host,
			user=self.db_user,
			passwd=self.db_password,
			database=self.db_default_db
		)
		self.curr = self.conn.cursor()

	def commit(self):
		self.conn.commit()

	def close(self):
		self.conn.close()
		print('closing database connection')

	def query(self, query_str):
		# input a str type query
		# returns 0 if execution fails
		# for select statements returns dictionary with keys 'header' (list of strings) and 'data' (list of list)

		result = {'header': [], 'data': []}

		try:
			self.curr.execute(query_str)
			all_data = self.curr.fetchall()

			# package data into dictionary
			for header in self.curr.description:
				result['header'].append(header[0])

			for entry in all_data:
				result['data'].append(entry)

		except Exception as error_msg:
			print("Error when executing query with error")
			print(error_msg)
			self.conn.rollback()
			return 0

		return result

	def insert(self, query_str):

		try:
			self.curr.execute(query_str)

		except Exception as error_msg:
			print("Error when executing query with error")
			print(error_msg)
			self.conn.rollback()
			return 0

		return 1

	def update(self, header, dataset):
		return 1
