from pypika import Query, functions
from database import DataBase
import ftplib
import config
from threading import get_ident


def insert_into_image_table(db, inspection_id, battery_side):

	cell_side = battery_side

	table = 'image'
	cols = [
		'inspection_id',
		'cell_side']

	data = [inspection_id, cell_side]

	q = Query.into(table).columns(*cols).insert(*data).get_sql(quote_char='`')

	# print('Image query string...')
	# print(q)

	insert_result = db.insert(q)

	result = 0
	if insert_result:
		result = 1

	return result


def insert_into_result_table(db, image_id, data):
	#  read csv data file

	table = 'result'
	data_list = []
	cols = [
		'image_id',
		'scan_id',
		'Peak1_Y',
		'Peak1_X',
		'Peak1_Z',
		'Peak2_Y',
		'Peak2_X',
		'Peak2_Z',
		'Valley_Y',
		'Valley_X',
		'Valley_Z',
		'DeltaZ_1',
		'DeltaZ_2']

	for row in data:
		tmp_row = row.split(',')
		tmp_row.insert(0, str(image_id))
		data_list.append(tmp_row)

	q = Query.into(table).columns(*cols).insert(*data_list).get_sql(quote_char='`')

	# print('Result query string...')

	insert_result = db.insert(q)

	result = 0
	if insert_result:
		result = 1

	return result


def insert_into_inspection_table(db, part_number, select_recipe, unit_id):

	table = 'inspection'
	serial_id = unit_id
	part_number_id = part_number
	recipe_id = select_recipe

	query = Query.into(table). \
		columns(
		'datetime',
		'serial_number',
		'part_number',
		'recipe_id',
		'equipment_id',
		'status'
	).insert(
		functions.Now(),
		serial_id,
		part_number_id,
		recipe_id,
		1,  # temporary placeholder until station list is established
		1   # status needs to be read from plc
	).get_sql(quote_char='`')

	# print('Inspection query string...')

	insert_result = db.insert(query)

	result = 0
	if insert_result:
		result = 1

	return result


class FTPRead:

	def __init__(self):
		self.data = b''

	def __call__(self, byte_data):
		self.data += byte_data


def etl(opc_ua_server, part_number, select_recipe, unit_id, battery_side_is_BS, image_count):

	db = DataBase()
	result = 0
	try:

		if battery_side_is_BS:
			file_name_side = 'BS'
		else:
			file_name_side = 'FS'

		if len(unit_id) < 2:
			raise Exception('Serial number is empty')

		#  pull data from FPT server

		ftp_connection = ftplib.FTP(config.FTP_HOST)
		ftp_connection.login(config.FTP_user_name)

		data = []
		for i in range(image_count):
			byte_data = FTPRead()
			result_file_name = 'xg\\result\\ResultData' + file_name_side + '_' + unit_id + '_' + str(i+1) + '.csv'
			ftp_connection.retrbinary('RETR ' + result_file_name, byte_data)
			tmp_data = byte_data.data.decode('ascii').split('\r')
			if i == 0:
				data = data + tmp_data[:-1]
			else:
				data = data + tmp_data[1:-1]

		#  start database transactions
		db.connect()
		print(f'Start ETL to database in thread {get_ident()}')

		print(f'[{get_ident()}] Inserting into inspection table')
		inspection_result = insert_into_inspection_table(db, part_number, select_recipe, unit_id)
		last_inspection_id = db.curr.lastrowid

		print(f'[{get_ident()}] Inserting into image table')
		image_result = insert_into_image_table(db, last_inspection_id, battery_side_is_BS)
		last_image_id = db.curr.lastrowid

		print(f'[{get_ident()}] Inserting into result table')
		result_result = insert_into_result_table(db, last_image_id, data[1:])

		if inspection_result and result_result and image_result:
			result = 1
			db.commit()
			print('turning off handshake')
			opc_ua_server.set_value_asynchronous('ns=4;s=MAIN.hmi.inp_etl_result', True)
			opc_ua_server.set_value_asynchronous('ns=4;s=MAIN.hmi.inp_etl_handshake', False)
		else:
			raise Exception(f'[{get_ident()}] One or more insert action failed')

	except Exception as e_msg:
		print(f"[{get_ident()}] Fail ETL with error message {e_msg}")
		opc_ua_server.set_value_asynchronous('ns=4;s=MAIN.hmi.inp_etl_result', False)
		opc_ua_server.set_value_asynchronous('ns=4;s=MAIN.hmi.inp_etl_handshake', False)
		return result

	finally:
		db.close()

	return result
