from elements import Element


class Structure(Element):
	def __init__(self, id, elements, html_type='div', **kwargs):
		text = ''
		self.elements = elements
		for element in elements:
			text += '\n' + element.html + '\n'
		Element.__init__(self, id, html_type, text, **kwargs)

	def update_elements(self, elements):
		self.elements = elements
		self.text = ''
		for element in self.elements:
			self.text += '\n' + element.html + '\n'
		self.update_html()


class StaticTemplate(Element):
	def __init__(self, id, template, arg_dictionary={}, html_type='div', **kwargs):
		self.template = template
		self.arg_dictionary = arg_dictionary
		Element.__init__(self, id, html_type, self.format_template, **kwargs)

	def update_template(self, template, arg_dictionary={}):
		self.template = template
		self.arg_dictionary = arg_dictionary
		self.update_html()

	def format_template(self):
		try:
			text = self.template.format(**self.arg_dictionary)
		except KeyError:
			text = self.template
		return text

	def update_arguments(self, arg_dictionary):
		self.arg_dictionary = arg_dictionary
		self.update_html()

	def update_html(self):
		self.text = self.format_template()
		Element.update_html(self)


class RepeatingTemplate(StaticTemplate):
	def __init__(self, id, template, arg_dictionary_list=[], **kwargs):
		self.arg_dictionary_list = arg_dictionary_list
		StaticTemplate.__init__(self, id, template, **kwargs)

	def format_repeat(self):
		return StaticTemplate.format_template(self)

	def format_template(self):
		text = '\n'
		for arg_dictionary in self.arg_dictionary_list:
			self.arg_dictionary = arg_dictionary
			text += self.format_repeat()
		return text

	def update_arguments(self, arg_dictionary_list):
		self.arg_dictionary_list = arg_dictionary_list
		self.update_html()


class PermissiveList(RepeatingTemplate):
	def __init__(self, id, arg_dictionary_list=[]):
		template = """
			<li class="m-1">
				<span style="width:25px" class="badge badge-{color}">{icon}</span>
				<text class="m-1">{description}</text>
			</li>
		"""
		RepeatingTemplate.__init__(self, id, template, arg_dictionary_list, classes='modal-body')

	def format_repeat(self):
		if self.arg_dictionary['is_active']:
			permissive_args = {}
			if self.arg_dictionary['status']:
				permissive_args['color'] = 'success'
				permissive_args['icon'] = '&#10003'
			else:
				permissive_args['color'] = 'danger'
				permissive_args['icon'] = '&#10005'
			permissive_args['description'] = self.arg_dictionary['description']
			self.arg_dictionary = permissive_args
			text = StaticTemplate.format_template(self)
		else:
			text = ''
		return text


class RepeatingElement(RepeatingTemplate):
	def __init__(self, id, element_args, arg_dictionary_list=[], **kwargs):
		self.element_args = element_args
		self.repeat_element_args = {}
		template = '{element}'
		RepeatingTemplate.__init__(self, id, template, arg_dictionary_list, **kwargs)

	def format_repeat(self):
		element = Element(**self.repeat_element_args)
		return element.html

	def format_template(self):
		text = '\n'
		for arg_dictionary in self.arg_dictionary_list:
			self.repeat_element_args = self.element_args.copy()
			self.repeat_element_args.update(arg_dictionary)
			text += self.format_repeat() + '\n'
		return text


class FieldsetLabelInput(StaticTemplate):
	def __init__(self, id, label_text, value_id, value='', input_type='text', disabled=False):
		template = """
			<div class="form-group row">
				<label for="{value_id}" class="col-form-label col-4">{label_text}</label>
				<div class="col-8">
					<input type="{input_type}" class="form-control" id="{value_id}" value="{value}">
				</div>
			</div>
		"""
		arg_dictionary = {'value_id': value_id,
						  'label_text': label_text,
						  'input_type': input_type,
						  'value': value}
		self.disabled = disabled
		StaticTemplate.__init__(self, id, template,
								arg_dictionary=arg_dictionary,
								html_type='fieldset')

	def create_parameters_string(self):
		if self.parameters:
			if self.disabled:
				self.parameters.update({'disabled': 'True'})
			else:
				try:
					del self.parameters['disabled']
				except KeyError:
					pass
				if self.parameters == {}:
					self.parameters = False
		else:
			if self.disabled:
				self.parameters = {'disabled': 'True'}

		parameters_string = Element.create_parameters_string(self)
		return parameters_string


class FieldsetRadio(StaticTemplate):
	def __init__(self, id, label_text, value_list=[], disabled=False):
		radio_template = """
		<div class = "form-check">
			<input class="form-check-input pt-0" type="radio" name="{name}", id="{value_id}" value="{value}" {checked}>
			<label for= "{value_id}" class="form-check-label pt-0">{label_text}</label>
		</div>
		"""
		for i, value_dict in enumerate(value_list):
			if value_dict['value']:
				value_dict['checked'] = 'checked'
			else:
				value_dict['checked'] = ''
			value_list[i] = value_dict
		radio_buttons = RepeatingTemplate('rmnRadioButtons', radio_template,
										  arg_dictionary_list=value_list,
										  classes="col-8")

		template = """
			<div class="form-group row">
				<label class="col-form-label col-4">{label_text}</label>
				{radio_buttons}
			</div>
		"""
		arg_dictionary = {'label_text': label_text,
						  'radio_buttons': radio_buttons.html}
		self.disabled = disabled
		StaticTemplate.__init__(self, id, template,
								arg_dictionary=arg_dictionary,
								html_type='fieldset')

	def create_parameters_string(self):
		if self.parameters:
			if self.disabled:
				self.parameters.update({'disabled': 'True'})
			else:
				try:
					del self.parameters['disabled']
				except KeyError:
					pass
				if self.parameters == {}:
					self.parameters = False
		else:
			if self.disabled:
				self.parameters = {'disabled': 'True'}

		parameters_string = Element.create_parameters_string(self)
		return parameters_string


class Block(StaticTemplate):
	def __init__(self, id, title, arg_dictionary={'body': ''}):
		arg_dictionary['title'] = title
		template = '''
		<div class='col m-2'>
			<h5>{title}</h5>
			<hr>
			{body}
		</div>
		'''
		StaticTemplate.__init__(
			self,
			id,
			template=template,
			arg_dictionary=arg_dictionary,
			classes='d-flex flex-row bg-light flex-grow-1 m-1'
		)



class ControlBlock(Block):
	def __init__(self, id, title, tags):
		self.title = title 	# text displayed at the top of the box
		self.tags = tags	# list of dictionaries describing each tags in the format:
							# {'name':'inp_one', 'val':0, 'type':'input'} where 'type' is 'input', 'trigger' or 'output'

		# StaticTemplate.__init__(
		# 	self,
		# 	id,
		# 	template = self.template,
		# 	arg_dictionary= {'title': title, 'body': body.html},
		# 	classes='d-flex flex-row bg-light flex-grow-1 m-1'
		# )
