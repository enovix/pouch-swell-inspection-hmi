from elements import Element


class Button(Element):
	def __init__(self, id, text='', disabled=False, outline=False, color='dark', btn_type='button', **kwargs):
		self.default_text = text
		self.outline = outline
		self.color = color
		self.check_color()
		persistent_parameters = {'type': btn_type}
		self.disabled = disabled
		Element.__init__(self, id, 'button', self.default_text, persistent_parameters=persistent_parameters, **kwargs)

	def create_parameters_string(self):
		if self.parameters:
			if self.disabled:
				self.parameters.update({'disabled': 'True'})
			else:
				try:
					del self.parameters['disabled']
				except KeyError:
					pass
				if self.parameters == {}:
					self.parameters = False
		else:
			if self.disabled:
				self.parameters = {'disabled': 'True'}

		parameters_string = Element.create_parameters_string(self)
		return parameters_string

	def create_class_string(self):
		if self.outline:
			outline_string = '-outline'
		else:
			outline_string = ''
		self.persistent_classes = 'btn btn{outline}-{color}'.format(outline=outline_string, color=self.color)
		classes_string = Element.create_class_string(self)
		return classes_string

	def check_color(self):
		if self.color in ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark']:
			return True
		else:
			raise ValueError(self.color + ' is not a valid color')

	def update_color(self, color):
		self.color = color
		self.check_color()
		self.update_html()

	def update_text(self, text, disabled=False):
		self.disabled = disabled
		Element.update_text(self, text)

	def disable(self):
		self.disabled = True
		self.update_html()

	def enable(self):
		self.disabled = False
		self.update_html()


class TSMButton(Button):
	def __init__(self, id, text='', disabled=True, classes='', **kwargs):
		Button.__init__(self, id, text, disabled=disabled, classes=classes, outline=False, color='light', **kwargs)

	def green(self, text):
		self.text = text
		self.disabled = True
		self.update_color('success')

	def yellow(self, text, disabled=True):
		self.text = text
		self.disabled = disabled
		self.update_color('warning')

	def red(self, text, disabled=True):
		self.text = text
		self.disabled = disabled
		self.update_color('danger')

	def enable(self, text):
		self.text = text
		self.disabled = False
		self.update_color('light')

	def disable(self):
		self.text = self.default_text
		self.disabled = True
		self.update_color('light')


class ModalButton(Button):
	def __init__(self, id, text, modal_target, **kwargs):
		persistent_data = {'toggle': 'modal', 'target': '#' + modal_target}
		Button.__init__(self, id, text, persistent_data=persistent_data, **kwargs)


class ControlButton(Button):
	def __init__(self, id, text='', classes='', disabled=True):
		if classes == '':
			classes = 'p-3 btn-block'
		Button.__init__(self, id, text, disabled=disabled, classes=classes, outline=False, color='dark')

	def green(self, text):
		self.text = text
		self.disabled = True
		self.outline = False
		self.update_color('success')

	def enable(self, text=''):
		if text != '':
			self.text = text
		else:
			self.text = self.default_text
		self.disabled = False
		self.outline = True
		self.update_color('dark')

	def disable(self,text=''):
		if text != '':
			self.text = text
		else:
			self.text = self.default_text
		self.disabled = True
		self.outline = False
		self.update_color('dark')


class ToggleButton(Button):
	''' a button with 2 states green and red

	Parameters: id, text='', classes='', disable=False
	'''
	def __init__(self, id, text='', classes='', disabled=False, toggleColor=False, toggleText=False, **kwargs):
		if toggleColor:
			self.toggleColor = toggleColor
		else:
			self.toggleColor = {True:'success', False:'light'}

		if toggleText:
			self.toggleText = toggleText
		else:
			self.toggleText = {True:'', False:''}

		Button.__init__(self, id, text, disabled=disabled, classes=classes, outline=False, color='light', **kwargs)

	def update(self, status):
		self.update_color(self.toggleColor.get(status))
		self.update_text(self.toggleText.get(status))

	def enable(self, text=''):
		if text != '':
			self.text = text
		else:
			self.text = self.default_text
		self.disabled = False
		self.update_color('dark')

	def disable(self, text=''):
		if text != '':
			self.text = text
		else:
			self.text = self.default_text
		self.disabled = True
		self.update_color('dark')