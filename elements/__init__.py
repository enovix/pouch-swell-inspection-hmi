import json


class Element:
	def __init__(self, id, html_type,
				 text='',
				 classes=False,
				 persistent_classes=False,
				 parameters=False,
				 persistent_parameters=False,
				 data=False,
				 persistent_data=False,
				 style=False,
				 persistent_style = False):

		self.id = id
		self.html_type = html_type
		self.text = text
		self.classes = classes
		self.persistent_classes = persistent_classes
		self.parameters = parameters
		self.persistent_parameters = persistent_parameters
		self.data = data
		self.persistent_data = persistent_data
		self.style = style
		self.persistent_style = persistent_style
		self.html = ''
		self.update_html()

	def update_text(self, text):
		self.text = text
		self.update_html()

	def update_classes(self, classes):
		self.classes = classes
		self.update_html()

	def update_data(self, data):
		self.data = data
		self.update_html()

	def create_parameters_string(self):
		parameters_string = ''
		if self.persistent_parameters:
			for key in self.persistent_parameters:
				parameters_string += ' {key}="{value}"'.format(key=key, value=self.persistent_parameters[key])
		if self.parameters:
			for key in self.parameters:
				parameters_string += ' {key}="{value}"'.format(key=key, value=self.parameters[key])
		return parameters_string

	def create_data_string(self):
		data_string = ''
		if self.persistent_data:
			for key in self.persistent_data:
				data_string += ' data-{key}="{value}"'.format(key=key, value=self.persistent_data[key])
		if self.data:
			for key in self.data:
				data_string += ' data-{key}="{value}"'.format(key=key, value=self.data[key])
		return data_string

	def create_class_string(self):
		classes = self.classes
		if self.persistent_classes and self.classes:
			classes = self.persistent_classes + ' ' + self.classes
		elif self.persistent_classes:
			classes = self.persistent_classes
		if classes:
			classes_string = ' class="%s"' % classes
		else:
			classes_string = ''
		return classes_string

	def create_style_string(self):
		if self.persistent_style and self.style:
			self.style = self.persistent_style + ' ' + self.style
		elif self.persistent_style:
			self.style = self.persistent_style
		if self.style:
			style_string = ' style="{}"'.format(self.style)
		else:
			style_string = ''
		return style_string

	def update_html(self):
		id_string = ' id="{}"'.format(self.id)
		header = self.html_type + id_string + self.create_class_string() + self.create_parameters_string() + \
			self.create_data_string() + self.create_style_string()
		body = self.text
		footer = self.html_type
		self.html = '<{header}>{body}</{footer}>'.format(header=header, body=body, footer=footer)

	def json(self):
		return json.dumps({'id': '#' + self.id, 'html': self.html})


class Alert(Element):
	def __init__(self, id, text):
		persistent_classes = "alert m-3"
		classes = "alert-success"
		persistent_parameters = {'role': 'alert'}
		parameters = {'hidden': 'True'}
		Element.__init__(self, id, 'div', text,
						 classes=classes,
						 persistent_classes=persistent_classes,
						 parameters=parameters,
						 persistent_parameters=persistent_parameters)

	def hide(self):
		self.parameters = {'hidden': 'True'}
		self.update_html()

	def green(self, text):
		self.text = text
		self.parameters = {}
		self.classes = "alert-success"
		self.update_html()

	def red(self, text):
		self.text = text
		self.parameters = {}
		self.classes = "alert-danger"
		self.update_html()

	def yellow(self, text):
		self.text = text
		self.parameters = {}
		self.classes = "alert-warning"
		self.update_html()


class Badge(Element):
	def __init__(self, id, text, a_good_thing=True):
		self.id = id
		self.text = text
		Element.__init__(self, id, 'span', text, classes='badge-secondary', persistent_classes='badge')
		self.a_good_thing = a_good_thing

	def update(self, status, text=False):
		if text:
			self.text = text
		if status:
			if self.a_good_thing:
				self.green()
			else:
				self.red()
		else:
			self.gray()

	def green(self):
		self.classes = 'badge-success'
		self.update_html()

	def red(self):
		self.classes = 'badge-danger'
		self.update_html()

	def yellow(self):
		self.classes = 'badge-warning'
		self.update_html()

	def gray(self):
		self.classes = 'badge-secondary'
		self.update_html()


class Table(Element):
	def __init__(self, id, table_data=[], headers=[], persistent_classes='table',
				 header_visible=True,
				 equal_width=False,
				 **kwargs):
		"""

		:param id:
		:param table_data: list of any type with __Str__
		:param headers: list of any type with __Str__
		:param persistent_classes:
		:param header_visible:
		:param kwargs:
		"""
		self.headers = headers
		self.table_data = table_data
		self.header_visible = header_visible
		self.equal_width = equal_width
		Element.__init__(self, id, 'table', self.generate_table(), persistent_classes=persistent_classes, **kwargs)

	def format_row(self, data_row, is_header=False):
		row_string = ""
		style = ''
		tr_class = ''
		td_class = ''

		if self.equal_width:
			tr_class = 'row'
			td_class = 'col'

		if not self.header_visible:
			style = 'display:none'

		for element in data_row:
			if is_header:
				row_string += '<th scope="col" class="text-center text-wrap" style="{style}">{data}</th>\n'\
					.format(data=element, style=style)
			else:
				try:
					row_string += f'<td class="text-center {td_class}">{element.html}</td>\n'
				except AttributeError:
					row_string += f'<td class="text-center {td_class}">{element}</td>\n'
		row_string = f'<tr class="{tr_class}">{row_string}</tr>\n'
		return row_string

	def generate_table(self):

		header_row = ''
		if self.header_visible:
			header_row = self.format_row(self.headers, is_header=True)

		data_rows = ''
		for data_row in self.table_data:
			data_rows += self.format_row(data_row)

		html_string = """ 
		<thead class="thead-light">
			{0}
		</thead>	
		<tbody>
			{1}
		</tbody>
		""".format(header_row, data_rows)
		return html_string

	def update_table_data(self, table_data):
		"""
		:param table_data: can take a list of list
		:return:
		"""
		self.table_data = table_data
		self.update_html()

	def update_html(self):
		self.text = self.generate_table()
		Element.update_html(self)


class Image(Element):
	def __init__(self, id, source, inverted=False, **kwargs):
		persistent_parameters = {'src': source}
		Element.__init__(self, id, 'img', persistent_parameters=persistent_parameters, **kwargs)
		if inverted:
			self.invert_svg()

	def invert_svg(self):
		self.persistent_style = "-webkit-filter: invert(1); filter: invert(1); width: 1rem; height: auto;"
		self.update_html()

	def update_html(self):
		id_string = ' id="{}"'.format(self.id)
		header = self.html_type + id_string + self.create_class_string() + self.create_parameters_string() + \
			self.create_data_string() + self.create_style_string()

		self.html = '<{header}>'.format(header=header)
