from elements import Element

# Element Class parameters ####################
# id,
# html_type,
# text='',
# classes=False,
# persistent_classes=False,
# parameters=False,
# persistent_parameters=False,
# data=False,
# persistent_data=False,
# style=False,
# persistent_style = False


def _check_color(f):
	def wrapper(color):
		if True:
			return f(color)
		else:
			raise ValueError(color + ' is not a valid color')
	return wrapper


class TabView(Element):
	def __init__(self, class_id='', direction='top', size=[], **kwargs):
		"""
		Generates a container with tab controlled pages

		call 'add_tab' to add information to the object
		call 'remove_tab' to remove a tab with its content from object
		call 'update_tab' to modify tab name and content

		:param class_id: defines the html id of this object. Used for updating object information.
						Should have a pre-written div element with the same id
		:param direction: defines the position of the tab relative to the content window (top,bottom,left,right)
		:param size: takes a [] of 2 number, 1st number defines the size of the tabs and
					2nd number defines the content window
		:param kwargs:
		"""

		self.tab_list = []
		self.content_list = []
		self.size = size
		self.tab_position = 'top'
		self.persistent_classes = 'd-flex bg-light flex-grow-1 m-1'
		self.classes = ''
		self.text = ''

		self.tabs_html = ''
		self.content_html = ''
		self.container_html = ''

		self.set_tab_position(direction)
		Element.__init__(self, id=class_id, html_type='div', classes=self.classes,
						 persistent_classes=self.persistent_classes, **kwargs)

	def set_tab_position(self, direction):
		if direction in ['top', 'bottom', 'left', 'right']:
			self.tab_position = direction
		else:
			raise Exception(f'{direction} is not a valid tab position')

	def _update_container_html(self):

		html_skeleton = '''
		<div class="{tab_container} tab-container" style="overflow: auto">
			<nav>
				<div class="nav nav-tabs {tab_group}" id="nav-tab" role="tablist" style="border-style:none">
					{tabs}
				</div>
			</nav>
		</div>
		<div class="{content_container} tab-content-container" style="overflow: auto">
			<div class="tab-content">
				{content}
			</div>
		</div>'''

		tab_container_size = ''
		content_container_size = ''

		if self.size and self.tab_position in ['right', 'left']:
			tab_container_size = f'col-{self.size[0]}'
			content_container_size = f'col-{self.size[1]}'

		dir_dict = {'top': {'top_container': 'flex-column', 'tab_container': '', 'tab_group': 'flex-row', 'content_container': ''},
					'bottom': {'top_container': 'flex-column-reverse', 'tab_container': '', 'tab_group': 'flex-row', 'content_container': ''},
					'left': {'top_container': 'flex-row', 'tab_container': tab_container_size, 'tab_group': 'flex-column', 'content_container': content_container_size},
					'right': {'top_container': 'flex-row', 'tab_container': tab_container_size, 'tab_group': 'flex-column', 'content_container': content_container_size}}

		dir_dict[self.tab_position]['tabs'] = '{tabs}'
		dir_dict[self.tab_position]['content'] = '{content}'

		self.classes = dir_dict[self.tab_position]['top_container']
		self.container_html = html_skeleton.format(**dir_dict[self.tab_position])

	def _update_tabs_html(self):
		"""
		loops through tab obj list to generate html.
		Updates self.tab_html
		:return:
		"""

		html = ''
		for tab in self.tab_list:
			html = html + '\n' + tab.html

		self.tabs_html = html

	def _update_content_html(self):
		"""
		loops through content obj list to generate html.
		Updates self.content_html
		:return:
		"""

		html = ''
		for content in self.content_list:
			html = html + '\n' + content.html

		self.content_html = html

	def remove_tab(self, tab_id=''):
		"""
		if no tab id is given all tabs will be cleared
		:param tab_id:
		:return:
		"""
		if not tab_id:
			self.tab_list = []
			self.content_list = []

	def add_tab(self, tab_name, content='', tab_color=False):
		"""
		appends 1 new tab with content to nav view
		:param tab_name: used for tab id and display name
		:param content:
		:param tab_color: takes in bootstrap standard color ie. bg-primary, text-primary
		:return:
		"""
		if self.tab_list:
			tab_obj = _Tab(tab_name, self.tab_position)
			content_obj = _TabContent(tab_name, content)
		else:
			tab_obj = _Tab(tab_name, self.tab_position, selected=True)
			content_obj = _TabContent(tab_name, content, selected=True)

		self.tab_list.append(tab_obj)
		self.content_list.append(content_obj)

		self.build_nav()

	def update_tab(self, current_tab_name, tab_name=False, content=False):
		"""
		updates a specific tab of the nav object

		:param current_tab_name: identifies the tab to modify
		:param tab_name: if pass updates tab name and id
		:param content: if pass update content of tab
		"""
		pass

	def build_nav(self):
		self._update_container_html()
		self._update_tabs_html()
		self._update_content_html()
		self.text = self.container_html.format(tabs=self.tabs_html, content=self.content_html)
		self.update_html()


class _Tab(Element):
	def __init__(self, id, direction='', selected=False, **kwargs):

		self.selected = selected
		self.color = ''
		self.id = id
		self.persistent_classes = f'nav-item nav-link {direction}'
		self.persistent_parameters = {'data-toggle':'tab',
									  'href':f'#{self.id}',
									  'role':'tab',
									  'aria-controls':f'{self.id}',
									  'aria-selected':'{}'.format(lambda selected: 'true' if selected else 'false')}

		Element.__init__(self, id=self.id + '-tab', html_type='a', text=self.id, persistent_classes=self.persistent_classes,
						 persistent_parameters=self.persistent_parameters,**kwargs)

	def create_class_string(self):

		selected_class = ''
		if self.selected:
			selected_class = 'active show'

		class_string = r'class="{}"'
		classes = f'{self.persistent_classes} {self.color} {selected_class}'

		return class_string.format(classes)

	@_check_color
	def update_color(self, color):
		self.color = color
		self.update_html()


class _TabContent(Element):
	def __init__(self, id, content='', selected=False, **kwargs):

		self.selected = selected
		self.color = ''
		self.id = id
		self.persistent_classes = 'tab-pane fade'
		self.persistent_parameters = {'role': 'tabpanel',
									  'labelledby': f'{self.id}-tab'}

		Element.__init__(self, id=self.id, html_type='div', persistent_classes=self.persistent_classes,
						 persistent_parameters=self.persistent_parameters, text=content, **kwargs)

	def create_class_string(self):

		selected_class = ''
		if self.selected:
			selected_class = 'active show'

		class_string = r'class="{}"'
		classes = f'{self.persistent_classes} {self.color} {selected_class}'

		return class_string.format(classes)

	@_check_color
	def update_color(self, color):
		self.color = color
		self.update_html()

	def update_content(self, html):
		self.text = html
		self.update_html()
