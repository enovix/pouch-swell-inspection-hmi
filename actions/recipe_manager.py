from elements import Table, Alert, Image, Element
from elements.button import Button, ModalButton
from elements.structure import StaticTemplate, Structure, FieldsetLabelInput, FieldsetRadio
from elements.nav import TabView
from opc_ua.objects import opc_ua_server
from app import socketio


class RMN:
	def __init__(self):
		print("init recipe manager")

		# ==== Input ==== #
		self.bp = 'ns=4;s=MAIN.recipe_manager.'  # basepath
		self.subscription = ''
		self.subscribe_to_opc_ua()

		self.recipe_list = []
		self.out_added = False
		self.out_edited = False
		self.out_deleted = False
		self.out_is_aborted = False
		self.out_duplicate = False
		self.out_recipe_list_full = False
		
		# ==== Page elements ==== #
		# recipe_table_headers = ['Part Number',
		# 						'Keyence Program',
		# 						'Z Height',
		# 						'X Length',
		# 						'X Start Position',
		# 						'Y Start Position',
		# 						'Y End Position',
		# 						'Scan Rows',
		# 						'Scan Pitch',
		# 						'Scan Offset',
		# 						'Image Count',
		# 						'Edit / Delete']
		# self.tbl_recipe_table = Table('rmnRecipeTable', [], '', persistent_classes='table col')
		self.recipe_tab_view = TabView('rmnRecipeTV', 'left', [2, 4], persistent_parameters={'style': 'height: 400px'})
		img_plus = Image('rmnPlusImage', "/static/icons/plus.svg", inverted=True)
		self.btn_add_recipe = ModalButton('rmnAddRecipeButton',
										  img_plus.html + '&nbspAdd New Recipe',
										  'rmnRecipeModal',
										  color='primary',
										  classes='btn-lg m-2 mx-4')
		img_pencil = Image('rmnPencilImage', "/static/icons/pencil.svg", inverted=True)
		self.btn_edit_recipe = ModalButton('rmnEditRecipeButton',
										   img_pencil.html,
										   'rmnRecipeModal',
										   color='primary')
		img_trash = Image('rmnTrashImage', "/static/icons/trash.svg", inverted=True)
		self.btn_delete_recipe = ModalButton('rmnDeleteRecipeButton',
											 img_trash.html,
											 'rmnRecipeModal',
											 color='danger')
		self.elm_recipe_modal_header = Element('rmnRecipeModalHeader', 'h5', classes="modal-title")
		self.str_recipe_modal_body = Structure('rmnRecipeModalBody', [], classes='modal-body')
		self.st_confirm_delete_text = StaticTemplate(
			'rmnConfirmDeleteText',
			'Are you sure you would like to <b class="text-danger">delete</b> the Recipe for Part Number - <b>{part_number}</b>',
			html_type='span')
		self.alrt_recipe_modal = Alert(id='rmnRecipeManagerAlert', text='')
		self.btn_recipe_modal_submit = Button('rmnSubmit', '', classes="btn-block", color='primary')
		
		# ==== Recipe Variables ==== #
		self.inp_part_number = ''
		self.inp_keyence_program = 0
		self.inp_z_height = 0
		self.inp_x_length = 0
		self.inp_x_start_position = 0
		self.inp_y_start_position = 0
		self.inp_y_end_position = 0
		self.inp_scan_row_count = 0
		self.inp_scan_row_pitch = 0
		self.inp_scan_row_offset = 0
		self.inp_image_count = 0

		# ==== Output ==== #
		self.output_json = ''

	def subscribe_to_opc_ua(self):
		subscription_period = 500  # (ms)
		subscription_paths = [self.bp + 'out_added',
							  self.bp + 'out_edited',
							  self.bp + 'out_deleted',
							  self.bp + 'out_duplicate',
							  self.bp + 'out_recipe_list_full',
							  'ns=4;s=MAIN.state_machine.out_aborted']

		self.reset_fb_path = self.bp + 'inp_reset'

		self.subscription = opc_ua_server.create_subscription(subscription_paths, subscription_period)

	def update_inputs(self):
		self.out_added = self.subscription.vars[self.bp + 'out_added'].val
		self.out_edited = self.subscription.vars[self.bp + 'out_edited'].val
		self.out_deleted = self.subscription.vars[self.bp + 'out_deleted'].val
		self.out_duplicate = self.subscription.vars[self.bp + 'out_duplicate'].val
		self.out_recipe_list_full = self.subscription.vars[self.bp + 'out_recipe_list_full'].val
		self.out_is_aborted = self.subscription.vars['ns=4;s=MAIN.state_machine.out_aborted'].val

	def generate_recipe_table(self, opc_ua_server):
		recipe_dict = opc_ua_server.get_child_values(self.bp + 'out_recipes')
		self.recipe_tab_view.remove_tab()
		for key in recipe_dict:
			if recipe_dict[key]['part_number'] != '':

				self.btn_edit_recipe.update_data(recipe_dict[key])	#  should only need partnumber info
				self.btn_delete_recipe.update_data(recipe_dict[key])  #  should only need partnumber info

				recipe_parameters = []
				table_cell_container = '<div data-type="{data_type}">{text}</div>'
				for item_key in recipe_dict[key]:
					value = recipe_dict[key][item_key]
					tmp_list = [table_cell_container.format(data_type='str', text=item_key),
								table_cell_container.format(data_type=type(value).__name__, text=value)]
					recipe_parameters.append(tmp_list)

				recipe_parameters.insert(0, [self.btn_edit_recipe.html, self.btn_delete_recipe.html])

				tbl_recipe_info = Table('', header_visible=False, equal_width=True, persistent_classes='table col')
				tbl_recipe_info.update_table_data(recipe_parameters)

				self.recipe_tab_view.add_tab(recipe_dict[key]['part_number'], tbl_recipe_info.html)

		self.recipe_tab_view.build_nav()
		socketio.emit('update', [self.recipe_tab_view.json()])

	def generate_modal_json_output(self):
		modal_json_output = [
			self.elm_recipe_modal_header.json(),
			self.str_recipe_modal_body.json(),
			self.alrt_recipe_modal.json(),
			self.btn_recipe_modal_submit.json()
		]

		return modal_json_output

	def generate_delete_modal(self, data):
		self.inp_part_number = str(data['part_number'])
		print(self.inp_part_number)
		# header
		self.elm_recipe_modal_header.update_text('Delete')
		# body
		self.st_confirm_delete_text.update_arguments({'part_number': self.inp_part_number})
		self.str_recipe_modal_body.update_elements([self.st_confirm_delete_text])
		# alert
		if self.out_is_aborted:
			self.alrt_recipe_modal.hide()
		else:
			self.alrt_recipe_modal.yellow('The machine must be in an aborted state to delete a recipe')
		# action button
		self.btn_recipe_modal_submit.update_color('danger')
		self.btn_recipe_modal_submit.update_data({'action': 'delete'})
		self.btn_recipe_modal_submit.update_text('Delete Recipe', disabled=not self.out_is_aborted)

		return self.generate_modal_json_output()

	def delete_recipe(self, opc_ua_server):
		print('confirm delete of recipe for PN ' + self.inp_part_number)
		opc_ua_server.set_value(self.bp + 'inp_part_number', self.inp_part_number)
		opc_ua_server.set_value(self.bp + 'inp_delete', True)

	def generate_edit_modal(self, data):
		self.update_inputs() #get the current Values of the PLC
		print(data)
		self.inp_part_number = data["part_number"]
		self.inp_keyence_program = int(data["keyence_program"])
		self.inp_z_height = float(data["z_height"])
		self.inp_x_length = float(data["x_length"])
		self.inp_x_start_position = float(data["x_start_position"])
		self.inp_y_start_position = float(data["y_start_position"])
		self.inp_y_end_position = float(data["y_end_position"])
		self.inp_scan_row_count = int(data["scan_row_count"])
		self.inp_scan_row_pitch = float(data["scan_row_pitch"])
		self.inp_scan_row_offset = float(data["scan_row_start_offset"])
		self.inp_image_count = int(data["image_count"])

		# header

		# body
		self.str_recipe_modal_body.update_elements([
			FieldsetLabelInput('rmnFSPartNumber', 'Part Number', 'rmnFormPartNumber', self.inp_part_number, disabled=True),
			FieldsetLabelInput('rmnFSKeyenceProgram', 'Keyence Program', 'rmnFormKeyenceProgram', self.inp_keyence_program),
			FieldsetLabelInput('rmnFSZHeight', 'Z Axis Height', 'rmnFormZHeight', self.inp_z_height),
			FieldsetLabelInput('rmnFSXLength', 'X Length', 'rmnFormXLength', self.inp_x_length),
			FieldsetLabelInput('rmnFSYStartPosition', 'Y Start Position', 'rmnFormYStartPosition', self.inp_y_start_position),
			FieldsetLabelInput('rmnFSYEndPosition', 'Y End Position', 'rmnFormYEndPosition', self.inp_y_end_position),
			FieldsetLabelInput('rmnFSXStartPosition', 'X Start Position', 'rmnFormXStartPosition', self.inp_x_start_position),
			FieldsetLabelInput('rmnFSScanRowCount', 'Scan Row Count', 'rmnFormRowCount', self.inp_scan_row_count),
			FieldsetLabelInput('rmnFSScanRowPitch', 'Scan Row Pitch', 'rmnFormRowPitch', self.inp_scan_row_pitch),
			FieldsetLabelInput('rmnFSScanRowOffset', 'Scan Start Offset', 'rmnFormRowOffset', self.inp_scan_row_offset),
			FieldsetLabelInput('rmnFSImageCount', 'Image Count', 'rmnFormImageCount', self.inp_image_count)
		])
		# alert
		if self.out_is_aborted:
			self.alrt_recipe_modal.hide()
		else:
			self.alrt_recipe_modal.yellow('The machine must be in an aborted state to edit a recipe')
		# action button
		self.btn_recipe_modal_submit.update_color('primary')
		self.btn_recipe_modal_submit.update_data({'action': 'edit'})
		self.btn_recipe_modal_submit.update_text('Edit Recipe', disabled=not self.out_is_aborted)

		return self.generate_modal_json_output()

	def edit_recipe(self, opc_ua_server):
		print('Part Number to edit is '+ str(self.inp_part_number))
		print('Z Height ' + str(self.inp_z_height))
		print('X Length ' + str(self.inp_x_length))
		print('X Start Position ' + str(self.inp_x_start_position))
		print('Y Start Position ' + str(self.inp_y_start_position))
		print('Y End Position ' + str(self.inp_y_end_position))
		print('scan row count ' + str(self.inp_scan_row_count))
		print('scan row pitch ' + str(self.inp_scan_row_pitch))
		print('scan row start offset ' + str(self.inp_scan_row_offset))
		print('Image Count ' + str(self.inp_image_count))

		opc_ua_server.set_value(self.bp + 'inp_recipe.part_number', self.inp_part_number)
		opc_ua_server.set_value(self.bp + 'inp_recipe.keyence_program', self.inp_keyence_program)
		opc_ua_server.set_value(self.bp + 'inp_recipe.z_height', self.inp_z_height)
		opc_ua_server.set_value(self.bp + 'inp_recipe.x_length', self.inp_x_length)
		opc_ua_server.set_value(self.bp + 'inp_recipe.y_start_position', self.inp_y_start_position)
		opc_ua_server.set_value(self.bp + 'inp_recipe.y_end_position', self.inp_y_end_position)
		opc_ua_server.set_value(self.bp + 'inp_recipe.x_start_position', self.inp_x_start_position)
		opc_ua_server.set_value(self.bp + 'inp_recipe.scan_row_count', self.inp_scan_row_count)
		opc_ua_server.set_value(self.bp + 'inp_recipe.scan_row_pitch', self.inp_scan_row_pitch)
		opc_ua_server.set_value(self.bp + 'inp_recipe.scan_row_start_offset', self.inp_scan_row_offset)
		opc_ua_server.set_value(self.bp + 'inp_recipe.image_count', self.inp_image_count)

		opc_ua_server.set_value(self.bp + 'inp_edit', True)

	def generate_add_modal(self):
		# header
		self.str_recipe_modal_body.update_elements([
			FieldsetLabelInput('rmnFSPartNumber', 'Part Number', 'rmnFormPartNumber', ''),
			FieldsetLabelInput('rmnFSKeyenceProgram', 'Keyence Program', 'rmnFormKeyenceProgram', ''),
			FieldsetLabelInput('rmnFSZHeight', 'Z Axis Height', 'rmnFormZHeight', ''),
			FieldsetLabelInput('rmnFSXLength', 'X Length', 'rmnFormXLength', ''),
			FieldsetLabelInput('rmnFSYStartPosition', 'Y Start Position', 'rmnFormYStartPosition', ''),
			FieldsetLabelInput('rmnFSYEndPosition', 'Y End Position', 'rmnFormYEndPosition', ''),
			FieldsetLabelInput('rmnFSXStartPosition', 'X Start Position', 'rmnFormXStartPosition', ''),
			FieldsetLabelInput('rmnFSScanRowCount', 'Scan Row Count', 'rmnFormRowCount', ''),
			FieldsetLabelInput('rmnFSScanRowPitch', 'Scan Row Pitch', 'rmnFormRowPitch', ''),
			FieldsetLabelInput('rmnFSScanRowOffset', 'Scan Start Offset', 'rmnFormRowOffset', ''),
			FieldsetLabelInput('rmnFSImageCount', 'Image Count', 'rmnFormImageCount', '')
			])
		self.elm_recipe_modal_header.update_text('Add')
		# body
		# alert
		if self.out_is_aborted:
			self.alrt_recipe_modal.hide()
		else:
			self.alrt_recipe_modal.yellow('The machine must be in an aborted state to add a new recipe')
		# action button
		self.btn_recipe_modal_submit.update_color('primary')
		self.btn_recipe_modal_submit.update_data({'action': 'add'})
		self.btn_recipe_modal_submit.update_text('Add Recipe', disabled=not self.out_is_aborted)

		return self.generate_modal_json_output()

	def add_recipe(self, opc_ua_server):

		print('Part Number to add is ' + str(self.inp_part_number))
		print('Z Height ' + str(self.inp_z_height))
		print('X Length ' + str(self.inp_x_length))
		print('X Start Position ' + str(self.inp_x_start_position))
		print('Y Start Position ' + str(self.inp_y_start_position))
		print('Y End Position ' + str(self.inp_y_end_position))
		print('scan row count ' + str(self.inp_scan_row_count))
		print('scan row pitch ' + str(self.inp_scan_row_pitch))
		print('scan row start offset ' + str(self.inp_scan_row_offset))
		print('Image Count ' + str(self.inp_image_count))

		opc_ua_server.set_value(self.bp + 'inp_recipe.part_number', self.inp_part_number)
		opc_ua_server.set_value(self.bp + 'inp_recipe.keyence_program', self.inp_keyence_program)
		opc_ua_server.set_value(self.bp + 'inp_recipe.z_height', self.inp_z_height)
		opc_ua_server.set_value(self.bp + 'inp_recipe.x_length', self.inp_x_length)
		opc_ua_server.set_value(self.bp + 'inp_recipe.x_start_position', self.inp_x_start_position)
		opc_ua_server.set_value(self.bp + 'inp_recipe.y_start_position', self.inp_y_start_position)
		opc_ua_server.set_value(self.bp + 'inp_recipe.y_end_position', self.inp_y_end_position)
		opc_ua_server.set_value(self.bp + 'inp_recipe.scan_row_count', self.inp_scan_row_count)
		opc_ua_server.set_value(self.bp + 'inp_recipe.scan_row_pitch', self.inp_scan_row_pitch)
		opc_ua_server.set_value(self.bp + 'inp_recipe.scan_row_start_offset', self.inp_scan_row_offset)
		opc_ua_server.set_value(self.bp + 'inp_recipe.image_count', self.inp_image_count)

		opc_ua_server.set_value(self.bp + 'inp_add', True)

	def reset_recipe_manager_fb(self):
		from opc_ua.objects import opc_ua_server
		opc_ua_server.set_value_asynchronous(self.reset_fb_path, True)

	def update_recipe_input(self, field, value):
		print(value)
		if field == 'recipe_part_number':
			self.inp_part_number = str(value)
			print('Part number set to ' + self.inp_part_number)

		elif field == 'keyence_program':
			self.inp_keyence_program = int(value)
			print('Keyence Program set to ' + str(self.inp_keyence_program))

		elif field == 'z_height':
			self.inp_z_height = float(value)
			print('Z Height set to ' + str(self.inp_z_height))

		elif field == 'x_start_position':
			self.inp_x_start_position = float(value)
			print('X Start Position set to ' + str(self.inp_x_start_position))

		elif field == 'x_length':
			self.inp_x_length= float(value)
			print('X length set to ' + str(self.inp_x_length))

		elif field == 'y_start_position':
			self.inp_y_start_position = float(value)
			print('Y Start Position set to ' + str(self.inp_y_start_position))

		elif field == 'y_end_position':
			self.inp_y_end_position = float(value)
			print('Y End Position set to ' + str(self.inp_y_end_position))

		elif field == 'scan_row_count':
			self.inp_scan_row_count = int(value)
			print('scan_row_count set to ' + str(self.inp_scan_row_count))

		elif field == 'scan_row_pitch':
			self.inp_scan_row_pitch = float(value)
			print('scan_row_pitch set to ' + str(self.inp_scan_row_pitch))

		elif field == 'scan_row_start_offset':
			self.inp_scan_row_offset = float(value)
			print('scan_row_start_offset set to ' + str(self.inp_scan_row_offset))

		elif field == 'image_count':
			self.inp_image_count = int(value)
			print('image_count set to ' + str(self.inp_image_count))

	def update_page_elements(self):
		if self.out_added:
			self.alrt_recipe_modal.green('Recipe Successfully Added')
			self.btn_recipe_modal_submit.disable()
			self.reset_recipe_manager_fb()
		elif self.out_duplicate:
			self.alrt_recipe_modal.red('Duplicate Recipe Found')
			self.reset_recipe_manager_fb()
		elif self.out_recipe_list_full:
			self.alrt_recipe_modal.red('Recipe List is full. Delete old recipes before adding more.')
			self.reset_recipe_manager_fb()
		elif self.out_deleted:
			self.alrt_recipe_modal.green('Recipe Successfully Deleted')
			self.btn_recipe_modal_submit.disable()
			self.reset_recipe_manager_fb()
		elif self.out_edited:
			self.alrt_recipe_modal.green('Recipe Successfully Edited')
			self.btn_recipe_modal_submit.disable()
			self.reset_recipe_manager_fb()

	def update(self):
		self.update_inputs()
		self.update_page_elements()
		self.json_encode()

	def json_encode(self):
		self.output_json = [
			self.alrt_recipe_modal.json(),
			self.btn_recipe_modal_submit.json(),
			# self.tbl_recipe_table.json()
			]
