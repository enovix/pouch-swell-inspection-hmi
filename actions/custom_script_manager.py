from opc_ua.objects import opc_ua_server
from scripts import cell_disposition
from threading import get_ident, Thread


def thread_with_call_back(target, t_args, call_back=None, cb_args=None):

	threading_script = Thread(target=target, args=t_args)
	threading_script.start()
	threading_script.join()

	if call_back is not None:
		if cb_args:
			call_back(cb_args)
		else:
			call_back()


class CSM:
	def __init__(self):
		print("init custom scripts manager")
		# ==== Variables ==== #

		self.bp = 'ns=4;s=MAIN.'  # basepath
		self.subscription = ''
		self.subscribe_to_opc_ua()

		self.active_serial_number = ''
		self.busy = False
		self.pre_disposition_trigger = OneShot()
		# ==== Page elements ==== #

	def subscribe_to_opc_ua(self):
		# ==== Input ==== #

		subscription_period = 500  # (ms)
		subscription_paths = [
			self.bp + 'hmi.out_etl_memory_roll_id_str',
			self.bp + 'hmi.inp_disposition_handshake',
			self.bp + 'hmi.out_disposition_trigger'
		]
		self.subscription = opc_ua_server.create_subscription(subscription_paths, subscription_period)

	def trigger_script(self, script_id, **kwargs):

		script_obj = False
		script_args = []
		call_back_args = []

		if script_id == 'cell_disposition':
			script_obj = cell_disposition.cell_disposition
			script_args = (kwargs['serial_number'],)
			call_back_args = ('ns=4;s=MAIN.hmi.inp_disposition_complete',)
		else:
			print(f'custom function {script_id} does not exist D:')

		if script_obj:
			async_thread = Thread(target=thread_with_call_back, args=(script_obj, script_args,  self.call_back, call_back_args))
			async_thread.start()

	def call_back(self, *args, **kwargs):
		print('script ran complete-------------------------------')
		print(f'with args: {args}')
		print(f'with kwargs: {kwargs}')
		for path in args[0]:
			print(path)
			opc_ua_server.set_value_asynchronous(path, True)

	def update_inputs(self):
		self.active_serial_number = self.subscription.vars[self.bp + 'hmi.out_etl_memory_roll_id_str'].val
		self.pre_disposition_trigger.set_value(self.subscription.vars[self.bp + 'hmi.out_disposition_trigger'].val)

		if self.pre_disposition_trigger.trigger:
			opc_ua_server.set_value_asynchronous(self.bp + 'hmi.inp_disposition_handshake', False)
			self.trigger_script('cell_disposition', serial_number=self.active_serial_number)

	def json_encode(self):
		self.output_json = [
		]

	def update(self):
		self.update_inputs()
		self.json_encode()


class OneShot:
	def __init__(self, value=0):
		self.curent_value = value
		self.previous_value = value
		self.trigger = False

	def set_value(self, new_value):
		self.curent_value = new_value
		if self.curent_value != self.previous_value and self.curent_value == 1:
			self.trigger = True
		else:
			self.trigger = False
		self.previous_value = self.curent_value
