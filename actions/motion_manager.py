from elements import Badge
from elements.button import ControlButton
from opc_ua.objects import opc_ua_server


class MTN:
	def __init__(self):
		print("init vision manager")

		self.bp = 'ns=4;s=MAIN.'  # basepath
		self.subscription = ''
		self.subscribe_to_opc_ua()

		# ==== Page elements ==== #
		self.bdg_motor1_enabled = Badge('mtnMotor1Enabled', 'Enabled')
		self.bdg_motor1_homing = Badge('mtnMotor1Homing', 'Homing')
		self.bdg_motor1_running = Badge('mtnMotor1Running', 'Running')
		self.bdg_motor2_enabled = Badge('mtnMotor2Enabled', 'Enabled')
		self.bdg_motor2_homing = Badge('mtnMotor2Homing', 'Homing')
		self.bdg_motor2_running = Badge('mtnMotor2Running', 'Running')
		self.bdg_web_running = Badge('mtnWebRunning', 'Web Moving')

		self.btn_enable_station = ControlButton('mtnEnableStation', 'Enable')
		self.btn_disable_station = ControlButton('mtnDisableStation', 'Disable')
		self.btn_home_station = ControlButton('mtnHomeStation', 'Home')
		self.btn_run_station = ControlButton('mtnRunStation', 'Run')
		self.btn_stop_station = ControlButton('mtnStopStation', 'Stop')
		self.btn_home_top_motor = ControlButton('mtnHomeTopMotor', 'Home')
		self.btn_run_top_motor = ControlButton('mtnRunTopMotor', 'Run')
		self.btn_stop_top_motor = ControlButton('mtnStopTopMotor', 'Stop')
		self.btn_home_bottom_motor = ControlButton('mtnHomeBottomMotor', 'Home')
		self.btn_run_bottom_motor = ControlButton('mtnRunBottomMotor', 'Run')
		self.btn_stop_bottom_motor = ControlButton('mtnStopBottomMotor', 'Stop')


		# ==== Output ==== #
		self.output_json = ''

	def subscribe_to_opc_ua(self):

		subscription_period = 500  # (ms)
		subscription_paths = [
			self.bp + 'brushing_station.out_enabled',
			self.bp + 'brushing_station.out_running',
			self.bp + 'brushing_station.out_motor_1_running',
			self.bp + 'brushing_station.out_motor_2_running',
			self.bp + 'brushing_station.out_web_running',
			self.bp + 'brushing_station.out_homing']

		self.subscription = opc_ua_server.create_subscription(subscription_paths, subscription_period)

	def update_inputs(self):
		self.bdg_motor1_enabled.update(self.subscription.vars[self.bp + 'brushing_station.out_enabled'].val)
		self.bdg_motor1_homing.update(self.subscription.vars[self.bp + 'brushing_station.out_homing'].val)
		self.bdg_motor1_running.update(self.subscription.vars[self.bp + 'brushing_station.out_motor_1_running'].val)
		self.bdg_motor2_enabled.update(self.subscription.vars[self.bp + 'brushing_station.out_enabled'].val)
		self.bdg_motor2_homing.update(self.subscription.vars[self.bp + 'brushing_station.out_homing'].val)
		self.bdg_motor2_running.update(self.subscription.vars[self.bp + 'brushing_station.out_motor_2_running'].val)
		self.bdg_web_running.update(self.subscription.vars[self.bp + 'brushing_station.out_web_running'].val)

		if self.subscription.vars[self.bp + 'brushing_station.out_enabled'].val:
			self.btn_enable_station.green('Enabled')
			self.btn_disable_station.enable()
			self.btn_home_station.enable()
			self.btn_run_station.enable()
			self.btn_stop_station.enable()
			self.btn_home_top_motor.enable()
			self.btn_run_top_motor.enable()
			self.btn_stop_top_motor.enable()
			self.btn_home_bottom_motor.enable()
			self.btn_run_bottom_motor.enable()
			self.btn_stop_bottom_motor.enable()

			if self.subscription.vars[self.bp + 'brushing_station.out_motor_1_running'].val \
				and self.subscription.vars[self.bp + 'brushing_station.out_motor_2_running'].val:

				self.btn_run_station.green('Running')
			else:
				self.btn_run_station.enable()
		else:
			self.btn_enable_station.enable()
			self.btn_disable_station.disable()
			self.btn_home_station.disable()
			self.btn_run_station.disable()
			self.btn_stop_station.disable()
			self.btn_home_top_motor.disable()
			self.btn_run_top_motor.disable()
			self.btn_stop_top_motor.disable()
			self.btn_home_bottom_motor.disable()
			self.btn_run_bottom_motor.disable()
			self.btn_stop_bottom_motor.disable()

	def json_encode(self):
		self.output_json = [
		self.bdg_motor1_enabled.json(),
		self.bdg_motor1_homing.json(),
		self.bdg_motor1_running.json(),
		self.bdg_motor2_enabled.json(),
		self.bdg_motor2_homing.json(),
		self.bdg_motor2_running.json(),
		self.btn_enable_station.json(),
		self.btn_disable_station.json(),
		self.btn_run_station.json(),
		self.btn_stop_station.json(),
		self.btn_home_station.json(),
		self.btn_run_top_motor.json(),
		self.btn_stop_top_motor.json(),
		self.btn_home_top_motor.json(),
		self.btn_run_bottom_motor.json(),
		self.btn_stop_bottom_motor.json(),
		self.btn_home_bottom_motor.json(),
		self.bdg_web_running.json()

		]

	def update(self):
		self.update_inputs()
		self.json_encode()
