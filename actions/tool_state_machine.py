from elements.button import TSMButton, ModalButton, Button, ToggleButton
from elements.structure import StaticTemplate, PermissiveList
from opc_ua.objects import opc_ua_server
from app import socketio
import json


class TSM:
	def __init__(self, instance_id, opc_base_path):
		print("init tool state machine")

		# ==== Input ==== #
		self.bp = opc_base_path
		self.subscription =''
		self.subscription_paths = [
						self.bp + 'out_faulted',
						self.bp + 'out_homing',
						self.bp + 'out_homed',
						self.bp + 'out_running',
						self.bp + 'out_aborting',
						self.bp + 'out_aborted',
						self.bp + 'out_pausing',
						self.bp + 'out_paused',
						self.bp + 'inp_lock_state']
		self.subscribe_to_opc_ua()

		# tsm state
		self.homing = False
		self.homed = False
		self.running = False
		self.aborting = False
		self.aborted = False
		self.pausing = False
		self.paused = False
		self.stopping = False
		self.stopped = False
		self.cycle_complete = False

		self.lock_tsm_actions = False

		# tsm flags
		self.step_faulted = False
		self.step_mode_enabled = False
		self.permissives_ok = False

		# tsm step
		self.tsm_step = 0
		self.tsm_step_description = ''

		# permissives
		self.permissives_paths = []
		self.create_permissive_paths()
		self.viewing_permissives = False

		# ==== Page elements ==== #
		self.btn_home = TSMButton(id='initiate'+instance_id, text='Initiate', classes='btn-lg mr-2',
								  parameters={'btn-type':'initiate'}, style='width:100px')
		self.btn_start = TSMButton(id='start'+instance_id, text='Start', classes='btn-lg mr-2',
								   parameters={'btn-type':'start'}, style='width:100px')
		self.btn_pause = TSMButton(id='pause'+instance_id, text='Pause', classes='btn-lg mr-2',
								   parameters={'btn-type':'pause'}, style='width:100px')
		self.btn_abort = TSMButton(id='abort'+instance_id, text='Abort', classes='btn-lg',
								   parameters={'btn-type':'abort'}, style='width:100px')

		self.btn_lock = ToggleButton(id='lock'+instance_id,
									 toggleColor={True: 'light', False: 'light'},
									 toggleText={
										 True: """<img src="/static/icons/lock-locked.svg" style='height: 25px' class="icon-red">""",
										 False: """<img src="/static/icons/lock-unlocked.svg" style='height: 25px' class="icon-green">"""},
									 parameters={'btn-type': 'lockTSM'})

		self.state_label_id = 'state'+instance_id
		self.tsm_state = 'state'
		# ==== Output ==== #
		self.output_json = ''

	def subscribe_to_opc_ua(self):
		subscription_period = 500  # (ms)
		self.subscription = opc_ua_server.create_subscription(self.subscription_paths, subscription_period)

	def create_permissive_paths(self):
		self.permissives_paths = []
		perm_bp = self.bp + 'inp_step_permissive[%s].'
		parameters = ['status', 'is_active', 'description']
		for i in range(10):
			self.permissives_paths += [perm_bp % i + parameter for parameter in parameters]

	def update_inputs(self):
		# tsm state
		self.step_faulted = self.subscription.vars[self.bp + 'out_faulted'].val
		self.homing = self.subscription.vars[self.bp + 'out_homing'].val
		self.homed = self.subscription.vars[self.bp + 'out_homed'].val
		self.running = self.subscription.vars[self.bp + 'out_running'].val
		self.aborted = self.subscription.vars[self.bp + 'out_aborted'].val
		self.paused = self.subscription.vars[self.bp + 'out_paused'].val
		self.lock_tsm_actions = self.subscription.vars[self.bp + 'inp_lock_state'].val

	def update_tsm_control_block(self):

		if self.lock_tsm_actions:
			self.btn_home.disable()
			self.btn_start.disable()
			self.btn_pause.disable()
			self.btn_abort.disable()
			self.btn_lock.update(True)
			return
		else:
			self.btn_lock.update(False)

		if self.step_faulted:  # step faulted is not a unique state but overrides other states
			self.btn_home.disable()
			self.btn_start.red('Faulted')
			self.btn_pause.disable()
			self.btn_abort.enable('Abort')
			self.tsm_state = 'faulted'

		elif self.homing:
			self.btn_home.green('Homing')
			self.btn_start.disable()
			self.btn_pause.disable()
			self.btn_abort.enable('Abort')
			self.tsm_state = 'homing'

		elif self.homed:
			self.btn_home.green('Ready')
			self.btn_start.enable('Start')
			self.btn_pause.disable()
			self.btn_abort.enable('Abort')
			self.tsm_state = 'homed'

		elif self.running:
			self.btn_home.disable()
			self.btn_start.green('Running')
			self.btn_pause.enable('Pause')
			self.btn_abort.enable('Abort')
			self.tsm_state = 'running'

		elif self.aborting:
			self.btn_home.disable()
			self.btn_start.disable()
			self.btn_pause.enable('Pause')
			self.btn_abort.red('Aborting')

		elif self.aborted:
			self.btn_home.enable('Home')
			self.btn_start.disable()
			self.btn_pause.disable()
			self.btn_abort.red('Aborted')
			self.tsm_state = 'aborted'

		elif self.pausing:
			self.btn_home.disable()
			self.btn_start.disable()
			self.btn_pause.yellow('Pausing')
			self.btn_abort.enable('Abort')

		elif self.paused:
			self.btn_home.disable()
			self.btn_start.enable('Resume')
			self.btn_pause.yellow('Paused')
			self.btn_abort.enable('Abort')
			self.tsm_state = 'paused'

		elif self.stopping:
			self.btn_home.disable()
			self.btn_start.disable()
			self.btn_pause.disable()
			self.btn_abort.enable('Abort')

		elif self.stopped:
			self.btn_home.disable()
			self.btn_start.disable()
			self.btn_pause.disable()
			self.btn_abort.enable('Abort')
			self.tsm_state = 'aborted'

		elif self.cycle_complete:
			self.btn_home.green('Complete')
			self.btn_start.enable('Start')
			self.btn_pause.disable()
			self.btn_abort.enable('Abort')

		else:
			self.btn_home.disable()
			self.btn_start.disable()
			self.btn_pause.disable()
			self.btn_abort.enable('Abort')
			self.tsm_state = 'Idle'

	def json_encode(self):
		self.output_json = [
			self.btn_home.json(),
			self.btn_start.json(),
			self.btn_pause.json(),
			self.btn_abort.json(),
			self.btn_lock.json()
		]

	def updated_state_label(self):
		command = []
		command.append(json.dumps({'selector': '#' + self.state_label_id + ' > span', 'attribute': 'text', 'value': self.tsm_state}))
		if self.tsm_state == 'faulted':
			command.append(json.dumps({'selector': '#' + self.state_label_id, 'attribute': 'css', 'value': ["background-color", "#dc3545"]}))
		elif self.tsm_state == 'homing':
			command.append(json.dumps({'selector': '#' + self.state_label_id, 'attribute': 'css', 'value': ["background-color", "#72c267"]}))
		elif self.tsm_state == 'homed':
			command.append(json.dumps({'selector': '#' + self.state_label_id, 'attribute': 'css', 'value': ["background-color", "#72c267"]}))
		elif self.tsm_state == 'running':
			command.append(json.dumps({'selector': '#' + self.state_label_id, 'attribute': 'css', 'value': ["background-color", "#72c267"]}))
		elif self.tsm_state == 'paused':
			command.append(json.dumps({'selector': '#' + self.state_label_id, 'attribute': 'css', 'value': ["background-color", "#ffc107"]}))
		elif self.tsm_state == 'aborted':
			command.append(json.dumps({'selector': '#' + self.state_label_id, 'attribute': 'css', 'value': ["background-color", "#dc3545"]}))
		else:
			command.append(json.dumps({'selector': '#' + self.state_label_id, 'attribute': 'css', 'value': ["background-color", "#6c757d"]}))

		socketio.emit('updateAttr', command)

	def update(self):
		if self.subscription:
			self.update_inputs()
		self.update_tsm_control_block()
		self.json_encode()
		self.updated_state_label()

	def subscribe_to_permissives(self):
		self.subscription.add_nodes(self.permissives_paths)
		self.update_inputs()
		self.viewing_permissives = True

	def unsubscribe_from_permissives(self):
		self.subscription.remove_nodes(self.permissives_paths)
		self.viewing_permissives = False
