from elements import Element
from elements.button import ModalButton
from elements.structure import StaticTemplate, RepeatingElement
from app import socketio
from opc_ua.objects import opc_ua_server


class UAW:
	def __init__(self):
		print("init user action wizard")
		# ==== Input ==== #
		self.bp = 'ns=4;s=MAIN.'
		self.instance_id = 0
		self.subscription = ''
		self.subscription_paths = [self.bp + f'sensors[{i}].hmi.out_test_name_request' for i in range(16)]
		self.subscription_paths = self.subscription_paths + \
								  [self.bp + f'sensors[{i}].out_test_name' for i in range(16)]
		self.subscription_paths = self.subscription_paths + \
								  [self.bp + f'sensors[{i}].inp_lock_state' for i in range(16)]

		self.subscribe_to_opc_ua()

		self.channel_lock_state = [False]*16
		self.set_test_id = [False]*16
		self.set_test_id_previous = [False]*16
		self.set_selected_channel = 0
		self.show_user_action = False
		self.open_test_names = [False]*16

		# ==== Page elements ==== #
		self.elmt_header = Element('uawHeader', 'h1', '', 'display-4')
		self.elmt_description = Element('uawDescription', 'p', '', 'lead')
		self.st_body = StaticTemplate('uawBody', '')
		self.btn_user_action_open = ModalButton('uawOpenModal',
												'<div class="d-flex justify-content-center">User Action</div>',
												'uawModal',
												classes='mt-auto mx-auto',
												outline=True,
												style='width:100px; height:80px',
												parameters={'data': self.instance_id})

		self.body_template_dict = self.define_html_templates()

		# ==== Output ==== #
		self.output_json = ''

	def subscribe_to_opc_ua(self):
		subscription_period = 500  # (ms)
		self.subscription = opc_ua_server.create_subscription(self.subscription_paths, subscription_period)

	def define_html_templates(self):
		set_test_id = """
		<form>
			<div class="form-row">
				<div class="col-8">
					<input type="text" class="form-control form-control-lg mx-1" id="uawTestID" value="">
				</div>
				<div class="col">
					<button type="modal-submit" id="uawSetTestID" class="btn btn-primary btn-lg btn-block" >Set Test ID</button>
				</div>
			</div>
		</form>
		"""

		channel_select = """
		<select class="display-4" id="uawChannelSelect">
			{}
		</select>
		"""

		templates = {
			'set_test_id': set_test_id,
			'channel_select': channel_select
		}

		return templates

	def update_inputs(self):
		self.set_test_id_previous = self.set_test_id.copy()
		for i in range(16):
			self.open_test_names[i] = self.subscription.vars[self.bp + f'sensors[{i}].out_test_name'].val
			self.set_test_id[i] = self.subscription.vars[self.bp + f'sensors[{i}].hmi.out_test_name_request'].val
			self.channel_lock_state[i] = self.subscription.vars[self.bp + f'sensors[{i}].inp_lock_state'].val
			if self.set_test_id_previous[i] != self.set_test_id[i] and self.set_test_id[i]:
				self.set_selected_channel = i
				self.show_user_action = True

		if not self.set_test_id[self.set_selected_channel]:
			self.set_selected_channel = 0

	def create_test_id_modal(self):
		option_text = '<option {0} value="{1}">{2}</option>'
		options = ''
		for i in range(16):
			if self.set_test_id[i] and i == self.set_selected_channel and not self.channel_lock_state[i]:
				options = options + option_text.format('selected', i, f'CHANNEL {i+1}') + '\n'
			elif self.set_test_id[i] and not self.channel_lock_state[i]:
				options = options + option_text.format('', i, f'CHANNEL {i+1}') + '\n'

		if options:
			self.elmt_header.update_text(self.body_template_dict['channel_select'].format(options))
			self.elmt_description.update_text('Enter Test Name or Identifier')
			self.st_body.update_template(self.body_template_dict['set_test_id'], {})
			if self.show_user_action:
				self.show_modal()
				self.show_user_action = False
			self.btn_user_action_open.update_color('danger')
		else:
			self.elmt_header.update_text('No User Actions Required')
			self.elmt_description.update_text('Equipment is not waiting on any user actions.')
			self.st_body.update_template('')
			self.hide_modal()
			self.btn_user_action_open.update_color('dark')

	def show_modal(self):
		socketio.emit('modal', '{"id": "#uawModal", "command": "show"}')

	def hide_modal(self):
		socketio.emit('modal', '{"id": "#uawModal", "command": "hide"}')

	def static_modal(self):
		socketio.emit('modal', '{"id": "#uawModal", "command": "static"}')

	def update_page_elements(self):
		self.create_test_id_modal()

	def json_encode(self):
		self.output_json = [
			self.elmt_header.json(),
			self.elmt_description.json(),
			self.btn_user_action_open.json(),
			self.st_body.json()
		]

	def update(self):
		self.update_inputs()
		self.update_page_elements()
		self.json_encode()


