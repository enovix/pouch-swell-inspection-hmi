from actions import tool_state_machine, user_action_wizard, tool_status_view, recipe_manager, motion_manager, custom_script_manager, modal_manager
from app import socketio
from opc_ua.objects import opc_ua_server


try:
	mm = modal_manager.MM()
	control_blocks = []
	tsm = []
	for i in range(16):
		tsm.append(tool_state_machine.TSM(instance_id=str(i+1), opc_base_path=f'ns=4;s=MAIN.sensors[{i}].'))

	control_blocks = control_blocks + tsm

	uaw = user_action_wizard.UAW()
	tsv = tool_status_view.TSV()

	control_blocks.append(uaw)
	control_blocks.append(tsv)

except Exception as ex:
	print("Error in control block init: ", ex)


def resubscribe_opc_tags():
	for block in control_blocks:
		block.subscribe_to_opc_ua()


def check_for_path(control_block, path):
	try:
		vars_list = control_block.subscription_paths
		if path in vars_list:
			return True
	except Exception as e:
		print('Error on check for path: ', e)


def opc_ua_datachange_handler(node, val):
	path = 'ns=%s;s=%s' % (node.nodeid.NamespaceIndex, node.nodeid.Identifier)

	for control_block in control_blocks:
		if check_for_path(control_block, path):
			# update all actions (opc values and other variable changes)
			# control blocks call element to update html and store into an json list
			control_block.update()

			# parse the json list and update the html files
			socketio.emit('update', control_block.output_json)


def opc_ua_refresh():
	try:
		for control_block in control_blocks:
			control_block.update()
			socketio.emit('update', control_block.output_json)
	except Exception as e:
		print("Error on refresh: ", e)


