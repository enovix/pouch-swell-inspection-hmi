from elements import Badge, Element
from elements.button import ToggleButton
from database import ETL
from opc_ua import AsynchronousThread
from opc_ua.objects import opc_ua_server
import config
from threading import get_ident, Thread
import time
from app import socketio


def check_opc_connection_status(opc_obj, btn_connection):
	print('Starting opc connection monitoring')

	while True:
		try:
			if opc_obj.event.is_set():
				break
			status = opc_obj.is_connected()
			if status:
				btn_connection.update(status=True)
			else:
				btn_connection.update(status=False)
				print('opc ua disconnected')
			socketio.emit('update', [btn_connection.json()])
			time.sleep(2)

			if not config.OPC_UA_ACTIVE:
				socketio.emit('update', [btn_connection.json()])
				break

		except Exception as e:
			print(f'[{get_ident()}] opc connection monitor error: {e}')
	print(f'[{get_ident()}] Terminating OPC Connection Monitor')


class TSV:
	def __init__(self):
		print("init tool state view")
		# ==== Variables ==== #
		self.etl_result = 0
		self.etl_running = False

		self.bp = 'ns=4;s=MAIN.'  # basepath
		self.subscription = ''
		self.subscription_paths = [
			self.bp + 'recipe_manager.out_active_recipe.part_number',
			self.bp + 'keyence.idle',
			self.bp + 'keyence.faulted',
			self.bp + 'hmi.out_x_axis_position_str',
			self.bp + 'hmi.out_y_axis_position_str',
			self.bp + 'hmi.out_z_axis_position_str',
			self.bp + 'recipe_manager.out_active_recipe_index',
			self.bp + 'hmi.out_etl_memory_roll_id_str',
			self.bp + 'hmi.out_battery_side_is_BS',
			self.bp + 'recipe_manager.out_active_recipe.image_count',
			self.bp + 'hmi.out_etl_trigger']
		self.subscribe_to_opc_ua()

		# ==== Page elements ==== #
		self.bdg_profilometer_enabled = Badge('tsvKeyenceEnabled', 'Enabled')
		self.bdg_profilometer_faulted = Badge('tsvKeyenceFaulted', 'Faulted', False)
		self.bdg_x_axis_position = Badge('tsvXAxisPosition', '0.000')
		self.bdg_y_axis_position = Badge('tsvYAxisPosition', '0.000')
		self.bdg_z_axis_position = Badge('tsvZAxisPosition', '0.000')
		self.bdg_ETL_result = Badge('tsvETLResult', 'Result')
		self.bdg_ETL_processing = Badge('tsvETLProcessing', 'Processing')
		self.elm_serial_number = Element('serialnumber', 'div', style='width:400px', persistent_classes='text-center lead')
		self.elm_part_number = Element('partnumber', 'div', style='width:400px', persistent_classes='text-center lead')

		btn_connection_text = ''

		self.btn_opc_connection = ToggleButton('btnConnectionToggle', btn_connection_text,
											   style = 'width:80px; height: 80px',
											   toggleColor={True:'light', False:'light'},
											   toggleText={True:"""<div><img class="icon-green" src="/static/icons/cloud.svg" style='height: 40px'></div>""",
														   False:"""<div><img class="icon-red" src="/static/icons/rain.svg" style='height: 40px'></div>"""
														   })

		# ==== Output ==== #

		self.output_json = ''

		check_opc_connection_thread = Thread(target=check_opc_connection_status,
											 args=(opc_ua_server, self.btn_opc_connection,))
		check_opc_connection_thread.start()

	def subscribe_to_opc_ua(self):
		# ==== Input ==== #
		subscription_period = 500  # (ms)
		self.subscription = opc_ua_server.create_subscription(self.subscription_paths, subscription_period)

	def trigger_etl(self):
		selected_recipe = self.subscription.vars.get(self.bp + 'recipe_manager.out_active_recipe_index').val
		unit_id = self.subscription.vars.get(self.bp + 'hmi.out_etl_memory_roll_id_str').val
		battery_side_is_BS = self.subscription.vars[self.bp + 'hmi.out_battery_side_is_BS'].val
		image_count = self.subscription.vars[self.bp + 'recipe_manager.out_active_recipe.image_count'].val
		part_number_name = self.subscription.vars[self.bp + 'recipe_manager.out_active_recipe.part_number'].val

		if config.ENVIRONMENT.lower() == 'production':
			async_etl_thread = AsynchronousThread(
				target=ETL.etl(opc_ua_server, part_number_name, selected_recipe, unit_id, battery_side_is_BS, image_count))
			async_etl_thread.start()

	def update_inputs(self):

		self.elm_part_number.update_text(self.subscription.vars[self.bp + 'recipe_manager.out_active_recipe.part_number'].val)
		self.elm_serial_number.update_text(self.subscription.vars.get(self.bp + 'hmi.out_etl_memory_roll_id_str').val)
		self.bdg_profilometer_enabled.update(self.subscription.vars[self.bp + 'keyence.idle'].val)
		self.bdg_profilometer_faulted.update(self.subscription.vars[self.bp + 'keyence.faulted'].val)
		self.bdg_x_axis_position.update(status=True,
										text=self.subscription.vars[self.bp + 'hmi.out_x_axis_position_str'].val)
		self.bdg_y_axis_position.update(status=True,
										text=self.subscription.vars[self.bp + 'hmi.out_y_axis_position_str'].val)
		self.bdg_z_axis_position.update(status=True,
											text=self.subscription.vars[self.bp + 'hmi.out_z_axis_position_str'].val)

		if self.subscription.vars[self.bp + 'hmi.out_etl_trigger'].val == 1:
			self.bdg_ETL_processing.update(status=True, text='Triggered')
			self.bdg_ETL_result.update(status=False, text='Result')
			if not self.etl_running:
				self.trigger_etl()
				self.etl_running = True
		else:
			self.etl_running = False
			self.bdg_ETL_processing.update(status=False, text='Waiting for Trig')
			self.bdg_ETL_result.update(status=False, text=self.etl_result.__str__())

	def json_encode(self):
		self.output_json = [
			self.bdg_profilometer_enabled.json(),
			self.bdg_profilometer_faulted.json(),
			self.bdg_x_axis_position.json(),
			self.bdg_y_axis_position.json(),
			self.bdg_z_axis_position.json(),
			self.bdg_ETL_processing.json(),
			self.bdg_ETL_result.json(),
			self.elm_part_number.json(),
			self.elm_serial_number.json()
		]

	def update(self):
		if self.subscription:
			self.update_inputs()
		self.json_encode()
