""" run flask app with following commands in the command line:

set FLASK_APP=main
flask run

"""

from app import app, socketio
from opc_ua.objects import opc_ua_server
import config


if __name__ == '__main__':
	try:
		socketio.run(app, host=config.HOST, port=config.PORT)  # run web server

	finally:
		opc_ua_server.event.set()
		opc_ua_server.disconnect()