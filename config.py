OPC_CLIENT_ADDR = 'opc.tcp://10.10.83.7:4840'
OPC_UA_ACTIVE = True
ENVIRONMENT = 'production'
db_host = ''
db_default_db = ''
equipment_name = ''
FTP_user_name = ''
FTP_HOST = ''


if ENVIRONMENT == 'production':
    HOST = '10.10.83.7'
    PORT = '5000'
    DASH_HOST = 'app10-dev'
    DASH_PORT = '5000'
elif ENVIRONMENT == 'dev1':
    print('CONFIG WARNING: Running in dev mode')
    HOST = '127.0.0.1'
    PORT = '5000'
    DASH_HOST = '127.0.0.1'
    DASH_PORT = '6969'
elif ENVIRONMENT == 'dev2':
    print('CONFIG WARNING: Running in dev mode')
    HOST = '127.0.0.1'
    PORT = '5001'
    DASH_HOST = '127.0.0.1'
    DASH_PORT = '6969'
else:
    HOST = ''
    PORT = ''
    print('CONFIG ERROR: Missing or invalid environment set.')
