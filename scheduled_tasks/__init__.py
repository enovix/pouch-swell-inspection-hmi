import threading



class ScheduledTask(threading.Thread):
	def __init__(self, target, period, *args, **kwargs):
		threading.Thread.__init__(self, target=target, args=args, kwargs=kwargs)
		self.period = period
		self._stopevent = threading.Event()

	def run(self):
		while not self._stopevent.isSet():
			self._target(*self._args, **self._kwargs)
			self._stopevent.wait(self.period)

	def join(self, timeout=None):
		self._stopevent.set()
		threading.Thread.join(self, timeout)


