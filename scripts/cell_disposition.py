import database
import numpy as np
from app import socketio
import json


def pull_stack_raw_data(db, serial_number):

	id_q = f'''
	select 
		max(i.inspection_id),
		im.cell_side
	from anode_to_cathode.inspection i
		join anode_to_cathode.image im on im.inspection_id = i.inspection_id
	where serial_number = '{serial_number}'
	group by cell_side
	order by cell_side
	'''

	data_q = '''
	select 
		i.inspection_id,
		i.serial_number,
		re.*
	from anode_to_cathode.inspection i
		join anode_to_cathode.image im on im.inspection_id = i.inspection_id
		join anode_to_cathode.result re on re.image_id = im.image_id
	where i.inspection_id = '{0}' and im.cell_side = {1}
	order by re.result_id
	'''

	try:
		db.connect()
		id_result = db.query(id_q)
		FS_result = db.query(data_q.format(id_result['data'][0][0], id_result['data'][0][1]))
		BS_result = db.query(data_q.format(id_result['data'][1][0], id_result['data'][1][1]))
	except Exception as e_msg:
		print(e_msg)
		return 0, 0
	finally:
		db.close()

	return FS_result, BS_result


def evaluate_disposition(data_set):
	result = {'maxDZ': ['N/A', 'N/A'],
			  'minDZ': ['N/A', 'N/A'],
			  'wavePair': ['N/A', 'N/A'],
			  'maxPitch': ['N/A', 'N/A'],
			  'minPitch': ['N/A', 'N/A'],
			  'dZSTD': ['N/A', 'N/A']}

	raw_data = data_set['data']
	data = np.array(raw_data)

	scan_row = 0
	image_id = 0
	peak_id = []
	x_pos = -999
	new_col = []
	maxDZ = -999
	minDZ = 999
	maxPitch = -999
	minPitch = 999
	all_dZ = []
	for i, row in enumerate(data):
		cur_x_pos = float(row[5])
		cur_image_id = int(row[4])

		if cur_image_id != image_id:
			image_id = cur_image_id
			scan_row = 0

		if abs(cur_x_pos - x_pos) >= 0.5:
			scan_row = scan_row + 1
			x_pos = cur_x_pos
			if len(peak_id) < scan_row:
				peak_id.append(0)

		peak_id[scan_row - 1] = peak_id[scan_row - 1] + 1

		new_col.append([scan_row, peak_id[scan_row - 1]])

		dZ1 = float(row[14])
		dZ2 = float(row[15])
		pitch = float(row[9]) - float(row[6])
		all_dZ.append(dZ1)
		all_dZ.append(dZ2)

		if max(dZ1, dZ2) > maxDZ:
			maxDZ = max(dZ1, dZ2)
			result['maxDZ'] = [maxDZ, f'R{scan_row}:C{peak_id[scan_row - 1]}']

		if min(dZ1, dZ2) < minDZ:
			minDZ = max(dZ1, dZ2)
			result['minDZ'] = [minDZ, f'R{scan_row}:C{peak_id[scan_row - 1]}']

		if pitch > maxPitch:
			maxPitch = pitch
			result['maxPitch'] = [maxPitch, f'R{scan_row}:C{peak_id[scan_row - 1]}']

		if pitch < minPitch:
			minPitch = pitch
			result['minPitch'] = [minPitch, f'R{scan_row}:C{peak_id[scan_row - 1]}']

	# new_data = np.append(data, new_col, axis=1)

	result['wavePair'] = [np.average(peak_id), peak_id]
	result['dZSTD'] = [np.std(all_dZ), round(np.average(all_dZ),4)]

	return result


def update_hmi_elements(sn, result, side):
	updates = []

	element_ids = [[{'id': 'dispoSN', 'limits': False},
					{'id': 'dispoMaxDZFS', 'limits': [-99, 99]},
					{'id': 'dispoMinDZFS', 'limits': [-99, 99]},
					{'id': 'dispoWPFS', 'limits': False},
					{'id': 'dispoMaxPitchFS', 'limits': [-99, 99]},
					{'id': 'dispoMinPitchFS', 'limits': [-99, 99]},
					{'id': 'dispoSTDFS', 'limits': [-99, 99]}],
					# back side limits
				   [{'id': 'dispoSN', 'limits': False},
					{'id': 'dispoMaxDZBS', 'limits': [-99, 99]},
					{'id': 'dispoMinDZBS', 'limits': [-99, 99]},
					{'id': 'dispoWPBS', 'limits': False},
					{'id': 'dispoMaxPitchBS', 'limits': [-99, 99]},
					{'id': 'dispoMinPitchBS', 'limits': [-99, 99]},
					{'id': 'dispoSTDBS', 'limits': [-99, 99]}]]

	state = 'danger'
	for i, res in enumerate(result):
		val = result[res]
		elm = element_ids[side][i+1]
		elm_id = elm['id']
		elm_limit = elm['limits']

		if elm_limit:
			if elm_limit[0] <= val[0] <= elm_limit[1]:
				state = 'success'
			else:
				state = 'danger'

		if 'dispoSTD' in elm_id:
			html_value = f'<td id="{elm_id}" class="badge-{state}" colspan="2">{round(val[0], 4)}</td>'
			cmd1 = {'id': f'#{elm_id}', 'html': html_value}
		elif 'dispoWP' in elm_id:
			if round(val[0], 4) % 1 > 0:
				state = 'danger'
			html_value = f'<td id="{elm_id}" class="badge-{state}" data-toggle="tooltip" title="{val[1]}" colspan="2">{round(val[0],4)}</td>'
			cmd1 = {'id': f'#{elm_id}', 'html': html_value}
		else:
			html_value = f'<td id="{elm_id}" class="badge-{state}">{round(val[0],4)}</td>'
			cmd1 = {'id': f'#{elm_id}', 'html': html_value}

		if 'dispoSTD' in elm_id:
			html_location = f'<td id="{elm_id}Loc" class="badge-{state}" colspan="2">{val[1]}</td>'
			cmd2 = {'id': f'#{elm_id}Loc', 'html': html_location}
		else:
			html_location = f'<td id="{elm_id}Loc" class="badge-{state}">{val[1]}</td>'
			cmd2 = {'id': f'#{elm_id}Loc', 'html': html_location}

		updates.append(json.dumps(cmd1))
		updates.append(json.dumps(cmd2))

	sn_html = f'<td id="dispoSN" scope="col" colspan="3" style="border-bottom: 1px solid #dee2e6">{sn}</td>'
	sn_cmd = {'id': '#dispoSN', 'html': sn_html}
	updates.append(json.dumps(sn_cmd))

	socketio.emit('update', updates)


def cell_disposition(serial_number):
	db = database.DataBase()
	# pull FS and BS data from database
	try:
		FS_data, BS_data = pull_stack_raw_data(db, serial_number)

		FS_result = evaluate_disposition(FS_data)
		update_hmi_elements(serial_number, FS_result, 0)
		BS_result = evaluate_disposition(BS_data)
		update_hmi_elements(serial_number, BS_result, 1)
	except Exception as e_msg:
		print(f'Failed during disposition script -- ERROR: {e_msg}')


