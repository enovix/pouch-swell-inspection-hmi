import config
from opcua import Client, ua
from threading import Thread, Event


def class_decorator(func):
	def f(*args, **kwargs):
		if config.OPC_UA_ACTIVE:  # allows opc_ua connection to be deactivated when starting the server
			return func(*args, **kwargs)
		else:
			print('The opc_ua connection is not active the function: [%s] did not run.' % func.__name__)
			return False
	return f


class Var:
	def __init__(self, path, node, val):
		self.path = path
		self.node = node
		self.val = val


class SubHandler(object):
	def __init__(self, obj):
		self.obj = obj

	def datachange_notification(self, node, val, data):
		# print("Python: New data change event", node, val)
		self.obj.update_val_dict(node, val)
		from actions import opc_ua_datachange_handler
		opc_ua_datachange_handler(node, val)

	def event_notification(self, event):
		print("Python: New event", event)


class SubscribedValue(object):
	def __setattr__(self, key, value):
		# print(key, value)
		super().__setattr__(key, value)

	def __init__(self, opcua_server, paths, period):
		self.opcua_server = opcua_server
		self.vars = {}
		self.__populate_vars(paths)
		self.period = period

		self.handler = SubHandler(self)
		self.sub = self.opcua_server.create_subscription(self.period, self.handler)
		self.handle = self.sub.subscribe_data_change([self.vars[path].node for path in self.vars])

	def __populate_vars(self, paths):
		for path in paths:
			var = Var(path, self.opcua_server.get_node(path), '')
			self.vars[path] = var

	def add_nodes(self, new_paths):
		for new_path in new_paths:
			if new_path not in self.vars:
				var = Var(new_path, self.opcua_server.get_node(new_path), '')
				self.vars[new_path] = var
		self.__subscribe()

	def remove_nodes(self, new_paths):
		for new_path in new_paths:
			try:
				del self.vars[new_path]
			except KeyError:
				pass
		self.__subscribe()

	def __subscribe(self):
		self.sub.delete()
		print("unsubscribed")

		self.sub = self.opcua_server.create_subscription(self.period, self.handler)
		self.handle = self.sub.subscribe_data_change([self.vars[path].node for path in self.vars])
		print("subscribed")

	def unsubscribe(self):
		self.sub.delete()
		print("unsubscribed")

	def update_val_dict(self, node, val):
		for path in self.vars:
			if self.vars[path].node == node:
				self.vars[path].val = val


class OpcUa:
	event = Event()

	def __init__(self):
		self.client = Client(config.OPC_CLIENT_ADDR)
		self.subscription = False
		self.period = -1
		self.paths = []

	@class_decorator
	def is_connected(self):
		if self.client.uaclient._uasocket._thread:
			return self.client.uaclient._uasocket._thread.is_alive()
		else:
			return False

	@class_decorator
	def connect(self):
		try:
			self.subscription = False
			self.client.connect()
			if self.is_connected():
				print("opcua server: %s connected" % config.OPC_CLIENT_ADDR)
			else:
				print(f'Failed to connect to opcua server: {config.OPC_CLIENT_ADDR}')
		except Exception as e_msg:
			print(f'Unable to connect to OPC Server with error : \n{e_msg} \n***')
			return False

	@class_decorator
	def disconnect(self):
		if self.is_connected():
			self.client.disconnect()
			self.subscription = False
			print("opcua server: %s disconnected" % config.OPC_CLIENT_ADDR)
		else:
			print(f'opcua server: {config.OPC_CLIENT_ADDR} already disconnected')

	@class_decorator
	def get_value(self, path):
		node = self.client.get_node(path)
		return node.get_value()

	@class_decorator
	def set_value(self, path, value):
		node = self.client.get_node(path)
		
		typ = type(value).__name__
		
		if typ == 'str':
			ua_typ = ua.VariantType.String
		elif typ == 'bool':
			ua_typ = ua.VariantType.Boolean
		elif typ == 'int':
			ua_typ = ua.VariantType.Int32
		elif typ == 'float':
			ua_typ = ua.VariantType.Double
		else:
			print('no handling for variable type: ' + typ)
			return
		ua_val = ua.DataValue(ua.Variant([value], ua_typ))
		node.set_data_value(ua_val)

	@class_decorator
	def set_value_asynchronous(self, path, value):
		t = AsynchronousThread(target=self.set_value, args=(path, value,))
		t.start()
		return True

	@class_decorator
	def get_child_values(self, path):
		def get_values(node):
			values = {}
			children = node.get_children()
			for child in children:
				name = child.nodeid.Identifier
				short_name = name.split('.')[-1]
				values[short_name] = get_values(child)
			if not children:
				values = node.get_value()
			return values

		object_node = self.client.get_node(path)
		return get_values(object_node)

	@class_decorator
	def create_subscription(self, paths, period=500):

		if 'ns=4;s=MAIN.sensors[1].out_running' in paths:
			asdf = 1

		if not self.subscription:
			self.subscription = SubscribedValue(self.client, paths, period)
			self.period = period
		else:
			self.subscription.add_nodes(paths)
			if period != self.period:
				raise Exception('A single OPCUA object can only handle a single sampling period')

		return self.subscription


class AsynchronousThread(Thread):
	def __init__(self, group=None, target=None, name=None, args=(), kwargs={}, Verbose=None):
		Thread.__init__(self, group, target, name, args, kwargs)
		self._return = None

	def run(self):
		if callable(self._target):
			self._return = self._target(*self._args, **self._kwargs)

	def join(self, *args):
		Thread.join(self, *args)
		return self._return
